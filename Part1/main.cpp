#include <fstream>
#include <iostream>
#include <string>
#include <vector>

typedef std::vector<int> IntVector;

void getModulesMass(IntVector *modules) {
  std::ifstream file;
  file.open("modules.txt");
  int mass;
  if (file.is_open()) {
    while (file >> mass) {
      modules->push_back(mass);
    }
    file.close();
  }
}

int getFuelRequired(IntVector *modules) {
  int fuel = 0;
  for (unsigned i = 0; i < modules->size(); i++) {
    fuel += modules->at(i) / 3 - 2;
  }
  return fuel;
}

int main() {
  IntVector modules;
  getModulesMass(&modules);
  // for (unsigned i = 0; i < modules.size(); i++) {
  //   std::cout << modules[i] << std::endl;
  // }
  int fuel = getFuelRequired(&modules);
  std::cout << "Fuel required is " << fuel << std::endl;
}
